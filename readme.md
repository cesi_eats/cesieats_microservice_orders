# Cesieats-microservice-article

## What is it ?

This is a microservice that allows you to create and manage articles.

## How to use it ?

### Development

To run the server in development mode, you can use the following command:
- npm install
- nodemon

### Production

#### Local deployment

You can deploy and run your app locally in production with the following command:
- npm install
- npm run deploy

#### Deploy using Docker

You can also deploy your app to a Docker container with the following command:
- docker build . -t \<user\>/<container_name>
- docker run -p <port>:<3000> -d  \<user\>/<container_name>

