import mongoose, { Schema, Document } from "mongoose";

export interface IOrder extends Document {
    userId: number;
    createdAt: Date;
    articles: Map<mongoose.Types.ObjectId,number>;
    address: string;
    restaurantId: mongoose.Types.ObjectId;
    delivererId: number,
    isDelivered: boolean,
}

const OrderSchema: Schema = new Schema({

    userId: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        required: true
    },
    articles: [{
        type: Map,
        of: mongoose.Schema.Types.ObjectId,
        required: true
    }],
    delivererId: {
        type: Number,
        required: false
    },
    isDelivered: {
        type: Boolean,
        required: true
    },
    address:{
        type: String,
        required: true
    },
    restaurantId:{
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
});

const Order = mongoose.model<IOrder>("Order", OrderSchema);
export { Order };
