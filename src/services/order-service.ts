import {Order} from "../models/Order";
import logger from "jet-logger";

// eslint-disable-next-line @typescript-eslint/ban-types
function getAll(callback:Function) {
    Order.find().then((orders) => {
        callback(orders);

    }).catch((err) => {
        if (err) {
            throw err;
        }
    })
}

function addNewOrder(userId : number, restaurantId : string, address: string, articles : Map<string,number>, callback:Function) {
    const newOrder = {
        userId: userId,
        createdAt: new Date(),
        address: address,
        restaurantId: restaurantId,
        articles: articles,
        isDelivered: false
    }

    const order = new Order(newOrder)

    order.save().then(() => {
        logger.info(`Order ${userId} created`);
    }).catch((err) => {
        if(err){
            throw err;
        }
    });
    callback(true, order);
}

function acceptOrder(orderId : string, delivererId : number, callback:Function) {
    Order.findOne({_id: orderId}).then((order) => {
        if(!order){
            callback(false, null);
        }
        else{
            order.delivererId = delivererId;
            order.save().then(() => {
                logger.info(`Order ${orderId} accepted`);
            }).catch((err) => {
                if(err){
                    throw err;
                }
            });
            callback(true, order);
        }
    });
}

// eslint-disable-next-line @typescript-eslint/ban-types
function getAllAvailable(callback:Function) {
    Order.where("delivererId").equals(null).then((orders) => {
        callback(orders);

    }).catch((err) => {
        if (err) {
            throw err;
        }
    })
}

function getByUserId(userId : number, callback:Function) {
    Order.find({userId: userId}).then((orders) => {
        callback(orders);
    }).catch((err) => {
        if (err) {
            throw err;
        }
    })
}

function getByDelivererId(delivererId : number, callback:Function) {
    Order.find({delivererId: delivererId}).then((orders) => {
        callback(orders);
    }).catch((err) => {
        if (err) {
            throw err;
        }
    })
}

function deliverOrder(orderId : string, callback:Function) {
    Order.findOne({_id: orderId}).then((order) => {
        if(!order){
            callback(false, null);
        }
        else{
            order.isDelivered = true;
            order.save().then(() => {
                logger.info(`Order ${orderId} delivered`);
            }).catch((err) => {
                if(err){
                    throw err;
                }
            });
            callback(true, order);
        }
    });
}


export default {
    getAll,
    addNewOrder,
    acceptOrder,
    getAllAvailable,
    getByUserId,
    getByDelivererId,
    deliverOrder
} as const;