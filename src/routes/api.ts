import { Router } from 'express';
import orderRouter from "@routes/order-router";



const apiRouter = Router();


apiRouter.use(orderRouter.baseRoute, orderRouter.router);


export default {
    apiRouter,
} as const;