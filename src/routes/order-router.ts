/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';
import logger from 'jet-logger';

import orderService from "@services/order-service";

const baseRoute = "/order";

// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: '/',
    add: '/',
    accept : '/accept',
    available : '/available',
    deliver: '/deliver/:orderId',
    getByUserId: '/user/:userId',
    getBydelivererId: '/deliverer/:delivererId',
} as const;


router.get(p.all, (req, res) => {
    orderService.getAll((orders:any) =>{
        res.status(200).json({success : true, data : orders});
    } );
})


router.get(p.available, (req, res) => {
    orderService.getAllAvailable((orders:any) =>{
        res.status(200).json({success : true, data : orders});
    } );
})

router.get(p.deliver, (req, res) => {
    const orderId = req.params.orderId;
    if (!orderId.match(/^[0-9a-fA-F]{24}$/)) {
        res.status(400).send({ success : false, error: 'Invalid id' });
        return;
    }
    orderService.deliverOrder(orderId, (success : boolean, order : any) =>{
        if (success) {
            res.status(200).json({success : true, data : order});
            return;
        }
        res.status(404).json({success : false, message: "Order not found"});
    } );
})


router.get(p.getByUserId, (req, res) => {
    let userId;
    try {
        userId = parseInt(req.params.userId);
    }
    catch (e){
        res.status(400).send({success : false,  error: 'Invalid id' });
        return;
    }
    if (!userId) {
        res.status(400).send({ success : false, error: 'Invalid id' });
        return;
    }

    orderService.getByUserId(userId,(orders:any) =>{
        res.status(200).json({success : true, data : orders});
    } );
})

router.get(p.getBydelivererId, (req, res) => {
    let delivererId;
    try {
        delivererId = parseInt(req.params.delivererId);
    }
    catch (e){
        res.status(400).send({ success : false, error: 'Invalid id' });
        return;
    }
    if (!delivererId) {
        res.status(400).send({ success : false, error: 'Invalid id' });
        return;
    }

    orderService.getByDelivererId(delivererId,(orders:any) =>{
        res.status(200).json({success : true, data : orders});
    } );
})


router.post(p.add, (req, res) => {
    const userId = req.body.userId;
    const restaurantId = req.body.restaurantId;
    const address = req.body.address;
    const articles = req.body.articles;
    if(!userId || !restaurantId || !address || !articles) {
        res.status(400).send({ success : false, error: 'Invalid request' });
        return;
    }

    orderService.addNewOrder(userId,restaurantId,address,articles, (order:any) =>{
        res.status(200).json({success : true, data : order});
    } );
})


router.post(p.accept, (req, res) => {
    const delivererId = req.body.delivererId;
    const orderId = req.body.orderId;
    orderService.acceptOrder(orderId, delivererId, (success: boolean, order: any) =>{
        if (success){
            res.status(200).json({success : true, data : order});
            return;
        }
        res.status(400).json({success : false, message: "Order not found"});
    } );
})





// Export default
export default {
    router,
    baseRoute
} as const;
